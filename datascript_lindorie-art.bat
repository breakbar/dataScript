@echo off

:: get time 
set year=%date:~6,4%
set month=%date:~3,2%
set day=%date:~0,2%
set hour=%time:~0,2%
set minute=%time:~3,2%

:: change month number's to month word's
if %month%==01 set month=January
if %month%==02 set month=February
if %month%==03 set month=March
if %month%==04 set month=April
if %month%==05 set month=May
if %month%==06 set month=Juin
if %month%==07 set month=July
if %month%==08 set month=August
if %month%==09 set month=September
if %month%==10 set month=October
if %month%==11 set month=November
if %month%==12 set month=december

:: if folders don't exist about the time actual
If Not Exist "L:/database/lindorie-art/%year%"	md "L:/database/lindorie-art/%year%"
If Not Exist "L:/database/lindorie-art/%year%/%month%"	md "L:/database/lindorie-art/%year%/%month%"
If Not Exist "L:/database/lindorie-art/%year%/%month%/%day%"	md "L:/database/lindorie-art/%year%/%month%/%day%"

:: get dump database to the path (databaseName folder)
set Timestamp=%year%/%month%/%day%/%hour%-%minute%
L:\wamp64\bin\mysql\mysql5.7.14\bin\mysqldump.exe -uroot -P3306 -h127.0.0.1 lindorie-art >	L:\database\lindorie-art\%Timestamp%_backup_lindorie-art.sql

:: get dump database to the path (global folder)
set Timestamp=%year%-%month%-%day%_%hour%-%minute%
L:\wamp64\bin\mysql\mysql5.7.14\bin\mysqldump.exe -uroot -P3306 -h127.0.0.1 lindorie_art >	L:\database\global\%Timestamp%_backupG_lindorie-art.sql