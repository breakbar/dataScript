# DataScript
Save in backup automatically ur db on windows

## Legend

- (path) -> path to ur directory
- (db) -> database name

## Batch file
Put this files in the same directory like _scripts/_

```
datascript.bat

datascript_(db).bat
datascript_(db).bat
datascript_(db).bat
datascript_(db).bat
...
```

## Code File
- datascript.bat

```
call datascript_(db).bat
call datascript_(db).bat
call datascript_(db).bat
call datascript_(db).bat
...
```
- datascript_(db).bat

```
@echo off

:: get time
set year=%date:~6,4%
set month=%date:~3,2%
set day=%date:~0,2%
set hour=%time:~0,2%
set minute=%time:~3,2%

:: change month number's to month word's
if %month%==01 set month=January
if %month%==02 set month=February
if %month%==03 set month=March
if %month%==04 set month=April
if %month%==05 set month=May
if %month%==06 set month=June
if %month%==07 set month=July
if %month%==08 set month=August
if %month%==09 set month=September
if %month%==10 set month=October
if %month%==11 set month=November
if %month%==12 set month=december

:: if folders don't exist about the time actual
If Not Exist "(path)\database\(db)\%year%"	md "(path)\database\(db)\%year%"
If Not Exist "(path)\database\(db)\%year%\%month%"	md "(path)\database\(db)\%year%\%month%"
If Not Exist "(path)\database\(db)\%year%\%month%\%day%"	md "(path)\database\(db)\%year%\%month%\%day%"

:: get dump database to the path (databaseName folder)
set Timestamp=%year%\%month%\%day%\%hour%-%minute%
(wamp64)\bin\mysql\mysql5.7.14\bin\mysqldump.exe -uroot -P3306 -h127.0.0.1 (-p if password) db >	(path)\%Timestamp%_backup_db.sql

:: get dump database to the path (global folder)
set Timestamp=%year%-%month%-%day%_%hour%-%minute%
(wamp64)\bin\mysql\mysql5.7.14\bin\mysqldump.exe -uroot -P3306 -h127.0.0.1 (-p if password) db >	(path)\global\%Timestamp%_backupG_db.sql
```
## Destination

Configure ur path destination like
```
_(path)\database\_
_(path)\database\(db)_
_(path)\database\global_
```

## Schedule this task

- Press _Win + R_ and write _TASKSCHD.MSC_
- Right click on the library and create a new folder and name it like _personal_
- Right click on this folder and create task like name in general write _datascript_
- Go to Action tab then new 
- Put in Program\script the to _datascript.bat_ then _OK_
- Go to trigger tab then new
- Define parameters like _Once_ 
- Start this today and define the clock to start like this format hh:00 or hh:30 > like if it's 18:12 set 18:30
- Check _synch_
- In advanced parameters check _repeat the task each_ then define ur interval like for exemple 30 and then we want the duration _forever_ then _OK_

## Author

**BreakBar labs**